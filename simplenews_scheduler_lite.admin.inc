<?php

/**
 * @file
 */

/**
 * Form settings for date format.
 */
function simplenews_scheduler_lite_settings($form, &$form_state) {
  $form = array();

  $form['simplenews_scheduler_lite_date_format'] = array(
    '#title' => t('Date format'),
    '#required' => TRUE,
    '#type' => 'radios',
    '#options' => array(
      'Y/m/d H:i' => date('Y/m/d H:i'),
      'm/d/Y H:i' => date('m/d/Y H:i'),
      'd/m/Y H:i' => date('d/m/Y H:i'),
    ),
    '#default_value' => variable_get('simplenews_scheduler_lite_date_format', 'Y/m/d H:i'),
  );

  return system_settings_form($form);
}
